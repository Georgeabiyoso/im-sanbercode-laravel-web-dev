@extends('layouts.master')

@section('title')
Halaman Biodata
@endsection

@section('content')
<form action="/home" method="POST">
        @csrf
        <label for="">Nama Depan</label> <br>
        <input type="text" name="fname"> <br> <br>
        <label for="">Nama Belakang</label> <br>
        <input type="text" name="lname"> <br> <br>

        <label>Gender:</label> <br><br>
        <input type="radio" name="gender" id="">Male <br>
        <input type="radio" name="gender" id="">Female <br>
        <input type="radio" name="gender" id="">Other <br><br>

        <label>Nationality:</label> <br><br>
        <select name="Nation" id="">
            <option value="1">Indonesian</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapore</option>
            <option value="4">Australia</option>
        </select><br><br>

        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="LanguageSpoken" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="LanguageSpoken" value="2">English <br>
        <input type="checkbox" name="LanguageSpoken" value="3">Other <br><br>

        <label>Bio:</label> <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="kirim">
    </form>
@endsection
