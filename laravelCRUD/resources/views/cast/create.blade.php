@extends('layouts.master')

@section('title')
    Halaman Tambah Cast
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="@error('name') is-invalid @enderror form-control" placeholder="Enter name">
  </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="number" name="umur" class="@error('umur') is-invalid @enderror form-control" placeholder="Enter umur">
  </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" name="bio" class="@error('bio`') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Enter Bio"></textarea>
  </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection