<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //tambah ke database table cast
        DB::table('cast')->insert([
            'name' => $request->input('name'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.read', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request->input('name'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio')
                ]
            );
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
