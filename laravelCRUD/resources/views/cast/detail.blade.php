@extends('layouts.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('content')

<h2>{{$cast->name}}</h2>
<h6>{{$cast->umur}}</h6>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-sm my-3 btn-secondary">Kembali</a>

@endsection