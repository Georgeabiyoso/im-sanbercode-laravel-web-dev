<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\BiodataController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'dashboard']);
Route::get('/daftar', [BiodataController::class, 'bio']);
Route::post('/home', [BiodataController::class, 'kirim']);

Route::get('/table', function(){
    return view('page.table');
}); 

Route::get('/data-table', function(){
    return view('page.datatable');
});

// Route::get('/master', function(){
//     return view('layouts.master');
// }); 