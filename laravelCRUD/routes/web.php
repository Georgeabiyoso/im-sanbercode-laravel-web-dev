<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'dashboard']);
Route::get('/daftar', [BiodataController::class, 'bio']);
Route::post('/home', [BiodataController::class, 'kirim']);

Route::get('/table', function(){
    return view('page.table');
}); 

Route::get('/data-table', function(){
    return view('page.datatable');
});

// Route::get('/master', function(){
//     return view('layouts.master');
// }); 

//MEMBUAT CRUD

//CREATE
Route::get('/cast/create', [CastController::class, 'create']);
//route ke database
Route::post('/cast', [CastController::class, 'store']);

//READ
Route::get('/cast', [CastController::class, 'index']);
//route untuk detail data
Route::get('/cast/{id}', [CastController::class, 'show']);

//UPDATE
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//route update database berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

//DELETE
Route::delete('/cast/{id}', [CastController::class, 'destroy']);